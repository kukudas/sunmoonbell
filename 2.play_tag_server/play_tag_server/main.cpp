#pragma comment (lib,"ws2_32")
#include <WinSock2.h>
#include <winsock.h>
#include <Windows.h>

#include <iostream>
#include <thread>
#include <vector>

using namespace std; 
#include "protocol.h"

HANDLE ghIOCP;   //iocp 핸들
SOCKET gsServer; // 글로벌 소켓

enum EVENTTYPE { E_RECV, E_SEND };

struct WSAOVERLAPPED_EX {
	WSAOVERLAPPED over;
	WSABUF wsabuf;
	unsigned char IOCP_buf[MAX_BUFF_SIZE];
	EVENTTYPE event_type;
};


// 클라 정보 구조체 
struct ClientInfo {
	int x, y;
	bool bConnected;

	SOCKET s;
	WSAOVERLAPPED_EX recv_over;
	unsigned char packet_buf[MAX_PACKET_SIZE];
	int prev_recv_size;
	int curr_packet_size;
};


ClientInfo gclients[MAX_USER];  //들어오는 유저들 일단 10명 제한


void error_display(char *msg, int err_no)
{
	WCHAR *lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	std::cout << msg;
	std::wcout << L"에러" << lpMsgBuf << std::endl;
	LocalFree(lpMsgBuf);
	while (true);
}
void InitializeServer()
{
	std::wcout.imbue(std::locale("korean"));

	// 모든 클라 처음에 접속 x 상태로 초기화
	for (int i = 0; i < MAX_USER; ++i) {
		gclients[i].bConnected = false;
	}

	// 2,2 버젼으로 시작
	WSADATA	wsadata;
	WSAStartup(MAKEWORD(2, 2), &wsadata);

	//iocp 생성
	ghIOCP = CreateIoCompletionPort(INVALID_HANDLE_VALUE, 0, NULL, 0);
	
	//소켓 생성
	gsServer = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);

	//주소 구조체 초기화
	SOCKADDR_IN ServerAddr;
	ZeroMemory(&ServerAddr, sizeof(SOCKADDR_IN));
	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_port = htons(MY_SERVER_PORT);
	ServerAddr.sin_addr.s_addr = INADDR_ANY;

	// bind  stl bind와 충돌되어서 :: 붙임
	::bind(gsServer, reinterpret_cast<sockaddr *>(&ServerAddr), sizeof(ServerAddr));

	// listen
	int res = listen(gsServer, SOMAXCONN);  //backlog: 서접속 가능한 클라의 개수. somaxconn은 지원 가능한 최댓값
	if (0 != res)
		error_display("Init Listen Error : ", WSAGetLastError());


}

void SendPacket(int cl, unsigned char *packet)
{
	std::cout << "Send Packet[" << static_cast<int>(packet[1]) << "] to Client : " << cl << std::endl;
	WSAOVERLAPPED_EX *send_over = new WSAOVERLAPPED_EX;
	ZeroMemory(send_over, sizeof(*send_over));
	send_over->event_type = E_SEND;
	memcpy(send_over->IOCP_buf, packet, packet[0]);
	send_over->wsabuf.buf = reinterpret_cast<CHAR *>(send_over->IOCP_buf);
	send_over->wsabuf.len = packet[0];
	

	// 진짜 보냄
	DWORD send_flag = 0;
	WSASend(gclients[cl].s, &send_over->wsabuf, 1, NULL,
		send_flag, &send_over->over, NULL);
}
void SendPutPlayerPacket(int target_client, int new_client)
{
	// 지역 변수로 사용자 정의 컨테이너를 만들고 
	// 해당 클라 아이디에 있는 정보를 받아서 그대로 뿌려줌
	sc_packet_put_player packet;
	packet.id = new_client;
	packet.size = sizeof(packet);
	packet.type = SC_PUT_PLAYER;
	packet.x = gclients[new_client].x;
	packet.y = gclients[new_client].y;

	SendPacket(target_client, reinterpret_cast<unsigned char *>(&packet));
}
void AcceptThread(){

	//클라 주소 초기화
	SOCKADDR_IN clientAddr;
	ZeroMemory(&clientAddr, sizeof(SOCKADDR_IN));
	clientAddr.sin_family = AF_INET;
	clientAddr.sin_port = htons(MY_SERVER_PORT);
	clientAddr.sin_addr.s_addr = INADDR_ANY;
	int addr_len = sizeof(clientAddr);

	while (true)
	{
		//  Accept 함수 
		SOCKET sClient = WSAAccept(gsServer, reinterpret_cast<sockaddr *>(&clientAddr), &addr_len, NULL, NULL);

		// Accept한 소켓이 하자일 때
		if (INVALID_SOCKET == sClient)
			error_display("Accept Thread Accept Error :", WSAGetLastError());
		//클라 접속 확인 출력 메세지 
		std::cout << "New Client Arrived! :";

		// 클라 접속 상태 확인후 빈곳에다가 고유 아이디 부여 
		int new_client_id = -1;
		for (int i = 0; i < MAX_USER; ++i)
			if (false == gclients[i].bConnected) {
				new_client_id = i;
				break;
			}

		//클라 접속 꽉 찻으면 소켓 닫고
		if (-1 == new_client_id) {
			closesocket(sClient);
			std::cout << "Max User Overflow!!!\n";
			continue;
		}

		// 클라 id 출력
		std::cout << "ID = " << new_client_id << std::endl;

		// 들어온 클라 정보 구조체 
		ZeroMemory(&gclients[new_client_id], sizeof(gclients[new_client_id]));
		gclients[new_client_id].s = sClient;
		gclients[new_client_id].bConnected = true;

		//들어온 클라 IOCP에 등록
		CreateIoCompletionPort(reinterpret_cast<HANDLE>(sClient), ghIOCP, new_client_id, 0);
		gclients[new_client_id].recv_over.event_type = E_RECV;
		gclients[new_client_id].recv_over.wsabuf.buf = reinterpret_cast<CHAR *>(gclients[new_client_id].recv_over.IOCP_buf);
		gclients[new_client_id].recv_over.wsabuf.len = sizeof(gclients[new_client_id].recv_over.IOCP_buf);

		// RECV 받아 
		DWORD recvFlag = 0;
		int ret = WSARecv(sClient, &gclients[new_client_id].recv_over.wsabuf,
			1, NULL, &recvFlag, &gclients[new_client_id].recv_over.over,
			NULL);

		//에러 메세지 출력
		if (0 != ret) {
			int err_no = WSAGetLastError();
			if (WSA_IO_PENDING != err_no)
				error_display("Recv Error in Accept Thread ", err_no);
		}

		// recv하고 클라가 출력할 수 있게 send 해준다. 
		SendPutPlayerPacket(new_client_id, new_client_id);

		// 루프를 돌면서 접속한 인원들을 검사하고 각각의 클라에게 상대방을 볼 수 있게 뿌려준다. 
		for (int i = 0; i < MAX_USER; ++i)
			if (true == gclients[i].bConnected)
				if (i != new_client_id) {
					SendPutPlayerPacket(i, new_client_id);
					SendPutPlayerPacket(new_client_id, i);
				}
	}

}
void SendRemovePlayerPacket(int target_client, int new_client)
{
	sc_packet_remove_player packet;
	packet.id = new_client;
	packet.size = sizeof(packet);
	packet.type = SC_REMOVE_PLAYER;

	SendPacket(target_client, reinterpret_cast<unsigned char *>(&packet));
}
void DisconnectClient(int cl)
{
	closesocket(gclients[cl].s);
	gclients[cl].bConnected = false;
	for (int i = 0; i < MAX_USER; ++i)
		if (true == gclients[i].bConnected) {
			SendRemovePlayerPacket(i, cl);
		}
}
void CloseServer(){

	closesocket(gsServer);
	WSACleanup();

}

void SendPositionPacket(int to, int object)
{
	sc_packet_pos packet;
	packet.id = object;
	packet.size = sizeof(packet);
	packet.type = SC_POS;
	packet.x = gclients[object].x;
	packet.y = gclients[object].y;

	SendPacket(to, reinterpret_cast<unsigned char *>(&packet));
}

void ProcessPacket(int cl, unsigned char *packet)
{
	std::cout << "Packet [" << packet[1] << "] from Client :" << cl << std::endl;
	switch (packet[1])
	{
	case CS_UP:
		if (0 < gclients[cl].y) gclients[cl].y--;
		break;
	case CS_DOWN:
		if (BOARD_HEIGHT - 1 > gclients[cl].y) gclients[cl].y++;
		break;
	case CS_LEFT:
		if (0 < gclients[cl].x) gclients[cl].x--;
		break;
	case CS_RIGHT:
		if (BOARD_WIDTH - 1 > gclients[cl].x) gclients[cl].x++;
		break;
	default:
		std::cout << "Unknown Packet Type from Client[" << cl << "]\n";
		exit(-1);
	}
	for (int i = 0; i < MAX_USER; ++i)
		if (true == gclients[i].bConnected)
			SendPositionPacket(i, cl);
}

void WorkerThread()
{
	while (true)
	{
		DWORD io_size;
		unsigned long long cl;
		WSAOVERLAPPED_EX *pOver;
		//GQCS로 받아버림
		BOOL is_ok = GetQueuedCompletionStatus(ghIOCP, &io_size, (PULONG_PTR)&cl,
			reinterpret_cast<LPWSAOVERLAPPED *>(&pOver), INFINITE);

		std::cout << "GQCS : Event   ";
		// 못받았으면..? 
		if (false == is_ok)
		{
			int err_no = WSAGetLastError();
			if (64 == err_no) DisconnectClient(cl);
			else error_display("GQCS Error : ", WSAGetLastError());
		}
		//이건 잘 모르겠다 . 
		if (0 == io_size) {
			DisconnectClient(cl);
			continue;
		}
///////////////////////////////////////////////////////////////////////////



		// 이벤트 타입이 RECV라면...받아야지? 
		if (E_RECV == pOver->event_type)
		{
			std::cout << "  data from Client :" << cl;
			int to_process = io_size;
			unsigned char *buf_ptr = gclients[cl].recv_over.IOCP_buf;
			unsigned char packet_buf[MAX_PACKET_SIZE];
			int psize = gclients[cl].curr_packet_size;
			int pr_size = gclients[cl].prev_recv_size;
		
		while (0 != to_process) 
			{
				if (0 == psize) psize = buf_ptr[0];
				if (psize <= to_process + pr_size) {
					memcpy(packet_buf, gclients[cl].packet_buf, pr_size);
					memcpy(packet_buf + pr_size, buf_ptr, psize - pr_size);
					ProcessPacket(static_cast<int>(cl), packet_buf);
					to_process -= psize - pr_size; buf_ptr += psize - pr_size;
					psize = 0; pr_size = 0;
					}
				else {
					memcpy(gclients[cl].packet_buf + pr_size, buf_ptr, to_process);
					pr_size += to_process;
					buf_ptr += to_process;
					to_process = 0;
					  }
			}
		//루프 다 돌았으면 RECV 합시다
		gclients[cl].curr_packet_size = psize;
		gclients[cl].prev_recv_size = pr_size;
		DWORD recvFlag = 0;
		int ret = WSARecv(gclients[cl].s, &gclients[cl].recv_over.wsabuf,
			1, NULL, &recvFlag, &gclients[cl].recv_over.over,
			NULL);
		//에러 처리
		if (0 != ret) {
			int err_no = WSAGetLastError();
			if (WSA_IO_PENDING != err_no)
				error_display("Recv Error in worker thread", err_no);
				}
		}



		// 이벤트가 SEND라면.....

		else if (E_SEND == pOver->event_type) {
			std::cout << "Send Complete to Client : " << cl << std::endl;
			if (io_size != pOver->IOCP_buf[0]) {
				std::cout << "Incomplete Packet Send Error!\n";
				exit(-1);
			}
			delete pOver;
		}
		else {
			std::cout << "Unknown GQCS Event Type!\n";
			exit(-1);
		}

	}
}
int main()
{

	std::vector<std::thread *> worker_threads;

	//초기화
	InitializeServer();

	// 워커 스레드 만들긔
	for (int i = 0; i < 6; ++i)
		worker_threads.push_back(new std::thread{ WorkerThread });

	// AcceptThread 만들기
	std::thread accept_thread{ AcceptThread };
	
	// thread 종료 대기 
	accept_thread.join();
	for (auto pth : worker_threads) { pth->join(); delete pth; }
	worker_threads.clear();

	//서버 문닫기
	CloseServer();

}