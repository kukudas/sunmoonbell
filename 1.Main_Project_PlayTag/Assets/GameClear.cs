﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameClear : MonoBehaviour {
    public GameObject effect;
    float startTime = 0;
	// Use this for initialization
	void Start () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        { 
            effect.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.2f, this.transform.position.z);
            Instantiate<Object>(effect);

            startTime = Time.time + 3.0f;
        }
    }

    // Update is called once per frame
    void Update () {
        if (startTime != 0)
        {
            if (startTime < Time.time)
                Application.LoadLevel("0_Lobby");
        }
    }
}
