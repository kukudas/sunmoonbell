﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetCameraMove : NetworkBehaviour {

    public GameObject player;

    public float offsetX = 0f;
    public float offsetY = 5f;
    public float offsetZ = 5f;

    Vector3 cameraPosition;

    // Use this for initialization
    void Start () {
		if(!isLocalPlayer)
        {
            Destroy(this);
            return;
        }
	}
	
	// Update is called once per frame
	void Update () {
        cameraPosition.z = player.transform.position.x + offsetX;
        cameraPosition.y = player.transform.position.y + offsetY;
        cameraPosition.x = player.transform.position.z + offsetZ;

        transform.position = cameraPosition;
    }
}
