﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageClear : MonoBehaviour {

    float startTime;
    // Use this for initialization
    void Start()
    {
        startTime = Time.time + 4.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (startTime < Time.time)
            Destroy(gameObject);
    }
}
