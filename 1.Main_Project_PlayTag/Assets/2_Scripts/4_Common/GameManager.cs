﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Net.Sockets;
using System.Net;
using System.Threading;

public class GameManager : Singleton<GameManager>
{
    // mapType -> 0 : School  // 1 : Park
    public int mapType = 0;
    public GameObject HowToPlay;
    // 공원맵 게임 오브젝트 설정
    public GameObject[] transMyChar = new GameObject[8];

    // Object
    public Camera MainCamera;
    public GameObject mr_goldenKey;
    public GameObject[] changeChar; // 변신될 오브젝트들
    public GameObject timerBarSliderOBJ; // setActive를 위해
    public Slider _timeBarSlider; // 슬라이더 변경을 위해
    public Slider stamina_TimeBar; // 뛰는 거 스테미나
    public GameObject[] live = new GameObject[5];
    public GameObject[] death = new GameObject[5];
    public int liveCount = 0;
    public GameObject sight;

    public GameObject temp; // 카메라 크기 변동 대비 temp
    public GameObject hammer; // 무기 아이템 먹으면 SetActive위해서

    public GameObject toilet_Left;
    public GameObject toilet_Right;
    public GameObject toilet_Little;

    public GameObject[] remainCount = new GameObject[4];
    public GameObject[] wall = new GameObject[6];
    public GameObject[] charHP = new GameObject[5];
    public int hpCount = 4;
    public AudioClip liveAudio;
    public AudioClip deathAudio;
    private AudioSource audio;

    public GameObject mine;

    public GameObject[] stateStrange = new GameObject[8];

    public GameObject[] stopTimer = new GameObject[2];
    public Text[] textTimer = new Text[2];


    private Vector3 cameraPos; // 카메라 포지션
    // Variable 
    private List<float> coordinate_x = new List<float>(); // 변기 x좌표 저장
    private float[] coordinate_y = new float[2];
    private List<float> coordinate_z = new List<float>(); // 변기 z좌표 저장
    [System.NonSerialized]
    public int wallCount = 0;
    [System.NonSerialized]
    public bool camera_col;
    [System.NonSerialized]
    public bool tagChange;
    float x, y, z;
    [System.NonSerialized]
    public int itemCount = 0; // 변신 아이템 변수, 변신 아이템 획득 시
    [System.NonSerialized]
    public int itemBoxCount = 0; // 아이템 변수, 박스 먹으면
    [System.NonSerialized]
    public bool ubae = false;
    [System.NonSerialized]
    private bool deathAudioPlay = false;
    public float changeTime;
    public float speedPlus;
    [System.NonSerialized]
    public bool itemOn = false; // 아이템 체크
    public float stamina = 3.0f;
    [System.NonSerialized]
    public bool goldenBoxOn = false; // 황금상자 먹으면 true
    [System.NonSerialized]
    public bool attackState = false; // 공격 상태 ( true면 충돌 시 잡는 사람이 죽음 )
    [System.NonSerialized]
    public bool objState = false;

    [System.NonSerialized]
    public bool winAnim = false;

    // 테스트용 UI
    public Text keyScoreText;
    public Text type1_Min;
    public Text type1_Sec;

    [System.NonSerialized]
    public float type1TimeStart;
    [System.NonSerialized]
    public float type1TimeNow;
    [System.NonSerialized]
    public int keyScore = 0;

    private float _changeTime; // 시간초 카운트(슬라이더)
    [System.NonSerialized]
    public int previtemCount; // 아이템 변수 저장


    // Need for System
    private float selfTimer = 0.0f;
    private float startTimer = 0.0f;

    private float changeTimer = 0.0f;


    //**********************진웅 시작 *********************///
    GameObject rabbit;
    // 소켓 생성
    Socket mySocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);


    // 플레이어 정보 디파인
    public PlayerInfo PlayInfo;

    byte[] buffer=null;
    //**********************진웅 끝***************************//



    // Use this for initialization
    void Start()
    {
        audio = GetComponent<AudioSource>();
        audio.loop = true;
        audio.clip = liveAudio;
        audio.Play();

       // deathAudio.Pause();


        stamina_TimeBar.maxValue = stamina;
        if (mapType == 0)
        {
            _timeBarSlider.maxValue = changeTime;
        }
        selfTimer = Time.time + 2.0f;
        startTimer = Time.time;
        _changeTime = changeTime; 
        if (mapType == 0)
        {
        cameraPos = MainCamera.transform.localPosition;
            Toilet();
        }

        if(mapType == 1) // 0 ~ n 랜덤 돌려서 캐릭터 생성
        {
            int randomNumber = Random.Range(0, 14);
            randomNumber = 0;
            transMyChar[randomNumber].transform.position = new Vector3(-2.0f,0.16f,0);
            Instantiate<Object>(transMyChar[randomNumber]);
            type1TimeStart = Time.time;
        }

        //***************진웅 시작**********************************

        //// 소켓 초기화 및  connect
        //mySocket.NoDelay = true;
        //mySocket.SendBufferSize = 0;
        //mySocket.Connect(new IPEndPoint(IPAddress.Loopback, 5354));

       // Instantiate(rabbit, new Vector3(0, 0, 0), Quaternion.identity);

        // ******************* 진웅 끝 *****************************

        StartCoroutine("updateRoutine");
      

    }

    // Update is called once per frame

    // 여기는 코드 충돌이 일어날 수 있으니 수정 시 말 부탁 (클라 曰)
    IEnumerator updateRoutine()
    {
        while (true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.LoadLevel("0_Lobby");
            }


            if (mapType == 1)
            {
                keyScoreText.text = "X " + keyScore.ToString();
                type1TimeNow = Time.time;

                type1_Min.text = "0"+((int)(type1TimeStart + 300f - type1TimeNow) / 60).ToString();
                type1_Sec.text = ((int)(type1TimeStart + 300f - type1TimeNow) % 60).ToString();
            }

            if (Input.GetKeyDown(KeyCode.F1))
                HowToPlay.SetActive(true);
            if (Input.GetKeyUp(KeyCode.F1))
                HowToPlay.SetActive(false);

            if (ubae == true && deathAudioPlay == false)
            {
                audio.clip = deathAudio;
                audio.Play();
                deathAudioPlay = true;
            }

            if(wallCount == 1)
            {
                for(int i = 0; i < 4; ++i)
                    Destroy(wall[i]);
            }
            else if(wallCount == 2)
            {
                for (int i = 3; i < 6; ++i)
                    Destroy(wall[i]);
            }

            // y는 고정 
            // x랑 z위치를 패킷으로 보내주면 받아서 생성
            // (클라 曰)
            //아이템 상자 뿌려두는 걸로 결정
            
            if (mapType == 1) // 공원 에서만 , 골든키
            {
                if (selfTimer < Time.time)
                {
                    x = Random.Range(-11.7f, 11.75f);
                    y = 0.071f; // 랜덤해서 1나오면 0.065f  2 나오면 1.44  3나오면 2.9로 ㄱㄱ 
                    //else if (_rand == 2)
                    //{
                    //    y = 1.538f;
                    //}
                    //else if (_rand == 3)
                    //{
                    //    y = 3.089f;
                    //}
                    z = Random.Range(-7.15f, 7f);
                    mr_goldenKey.transform.position = new Vector3(x, y, z);
                    Instantiate<Object>(mr_goldenKey);
                    selfTimer = Time.time + 3.0f;
                }

                //if (itemBoxCount != 0)
                //    Item();
            }
            
            if (itemBoxCount != 0)
                Item();
            if (ubae == true)
            {
                //live.SetActive(false);
                //death.SetActive(true);
            }

            

            stamina_TimeBar.value = stamina;
            yield return null;

        }
    }



    // 볼 필요 없음 ( 아이템 구현 부분 ) (클라 曰)
    void Item()
    {
        //ITEM 구현
        if (itemBoxCount == 1) // 크기 2배 증가
        {
            if (itemOn == false)
            {
                MainCamera.transform.parent = temp.transform;
                changeChar[0].transform.localScale = new Vector3(2.0f, 2.0f, 2.0f);
                MainCamera.transform.parent = changeChar[0].transform;

                changeTimer = Time.time + changeTime;
                if (mine == changeChar[0])
                    stateStrange[1].SetActive(true);
                itemOn = true;
            }

            //슬라이더
            if (mine == changeChar[0])
            {
                if ((changeTimer - Time.time) >= 0)
                {
                    timerBarSliderOBJ.SetActive(true);
                    _changeTime = (changeTimer - Time.time);
                    _timeBarSlider.value = _changeTime;
                }
                else
                {
                    //timerBarSliderOBJ.SetActive(false);
                    _timeBarSlider.value = 10;

                }
            }
            if (changeTimer < Time.time)
            {
                if (itemOn == true)
                {
                    MainCamera.transform.parent = temp.transform;
                    changeChar[0].transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    MainCamera.transform.parent = changeChar[0].transform;
                    itemBoxCount = 0;
                    changeTimer = 0;
                    stateStrange[1].SetActive(false);

                    itemOn = false;
                }
            }

        }

        if (itemBoxCount == 2) // 크기 2배 감소
        {
            if (itemOn == false)
            {
                MainCamera.transform.parent = temp.transform;
                changeChar[0].transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
                MainCamera.transform.parent = changeChar[0].transform;

                changeTimer = Time.time + changeTime;
                if (mine == changeChar[0])
                    stateStrange[0].SetActive(true);

                itemOn = true;
            }

            //슬라이더
            if (mine == changeChar[0])
            {
                if ((changeTimer - Time.time) >= 0)
                {
                    timerBarSliderOBJ.SetActive(true);
                    _changeTime = (changeTimer - Time.time);
                    _timeBarSlider.value = _changeTime;
                }
                else
                {
                    // timerBarSliderOBJ.SetActive(false);
                    _timeBarSlider.value = 10;

                }
                if (changeTimer < Time.time)
                {
                    if (itemOn == true)
                    {
                        MainCamera.transform.parent = temp.transform;
                        changeChar[0].transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                        MainCamera.transform.parent = changeChar[0].transform;
                        itemBoxCount = 0;
                        changeTimer = 0;
                        stateStrange[0].SetActive(false);

                        itemOn = false;
                    }
                }
            }
        }
        if (itemBoxCount == 4) // 속도 증가
        {
            if (itemOn == false)
            {
                speedPlus = 0.4f;
                changeTimer = Time.time + changeTime;
                stateStrange[4].SetActive(true);

                itemOn = true;
            }

            //슬라이더
            if ((changeTimer - Time.time) >= 0)
            {
                timerBarSliderOBJ.SetActive(true);
                _changeTime = (changeTimer - Time.time);
                _timeBarSlider.value = _changeTime;
            }
            else
            {
                // timerBarSliderOBJ.SetActive(false);
                _timeBarSlider.value = 10;

            }
            if (changeTimer < Time.time)
            {
                if (itemOn == true)
                {
                    speedPlus = -0.4f;
                    itemBoxCount = 0;
                    changeTimer = 0;
                    stateStrange[4].SetActive(false);

                    itemOn = false;
                }
            }

        }
        if (itemBoxCount == 5) // 속도 1.4배 감소
        {
            if (itemOn == false)
            {
                speedPlus = -0.4f;
                changeTimer = Time.time + changeTime;
                stateStrange[5].SetActive(true);

                itemOn = true;
            }

            //슬라이더
            if ((changeTimer - Time.time) >= 0)
            {
                timerBarSliderOBJ.SetActive(true);
                _changeTime = (changeTimer - Time.time);
                _timeBarSlider.value = _changeTime;
            }
            else
            {
                // timerBarSliderOBJ.SetActive(false);
                _timeBarSlider.value = 10;

            }
            if (changeTimer < Time.time)
            {
                if (itemOn == true)
                {
                    speedPlus = 0.4f;
                    itemBoxCount = 0;
                    changeTimer = 0;
                    stateStrange[5].SetActive(false);

                    itemOn = false;
                }
            }
        }
        // 캐릭터 사물로 변신
        if (itemBoxCount == 6)
        {
            if (itemCount != 0)
            {
                previtemCount = itemCount;
                changeTimer = Time.time + changeTime; //
                objState = true;

                changeChar[itemCount].transform.position = new Vector3(changeChar[0].transform.position.x, changeChar[0].transform.position.y + 0.3f, changeChar[0].transform.position.z);


                changeChar[itemCount].transform.rotation = changeChar[0].transform.rotation;
                //changeChar[itemCount].SetActive(true);
                MainCamera.transform.parent = changeChar[itemCount].transform;
                changeChar[0].transform.position = new Vector3(-100f, -100f, -100f);
                //changeChar[0].SetActive(false);
                stateStrange[6].SetActive(true);

                itemCount = 0;
            }
            if ((changeTimer - Time.time) >= 0)
            {
                timerBarSliderOBJ.SetActive(true);
                _changeTime = (changeTimer - Time.time);
                _timeBarSlider.value = _changeTime;
            }
            else
            {
                // timerBarSliderOBJ.SetActive(false);
                _timeBarSlider.value = 10;

            }
            // 변신 해제
            if (changeTimer != 0)
            {
                if (changeTimer < Time.time)
                {
                    objState = false;
                    changeChar[0].transform.position = new Vector3(changeChar[previtemCount].transform.position.x, changeChar[previtemCount].transform.position.y - 0.205708f, changeChar[previtemCount].transform.position.z);
                    changeChar[0].transform.rotation = changeChar[previtemCount].transform.rotation;
                    //changeChar[0].SetActive(true);
                    MainCamera.transform.parent = changeChar[0].transform;
                    changeChar[previtemCount].transform.position = new Vector3(-100f, -99f, -100f);
                    //changeChar[previtemCount].SetActive(false);
                    MainCamera.transform.localPosition = new Vector3(cameraPos.x, cameraPos.y, cameraPos.z);
                    stateStrange[6].SetActive(false);

                    changeTimer = 0;
                }
            }
        }
        if (itemBoxCount == 3) // 시야 감소
        {
            if (mine == changeChar[0])
            {
                if (itemOn == false)
                {
                    changeTimer = Time.time + changeTime;
                    stateStrange[2].SetActive(true);
                    sight.SetActive(true);
                    itemOn = true;
                }

                //슬라이더
                if ((changeTimer - Time.time) >= 0)
                {
                    timerBarSliderOBJ.SetActive(true);
                    _changeTime = (changeTimer - Time.time);
                    _timeBarSlider.value = _changeTime;
                }
                else
                {
                    // timerBarSliderOBJ.SetActive(false);
                    _timeBarSlider.value = 10;
                }
                if (changeTimer < Time.time)
                {
                    if (itemOn == true)
                    {
                        itemBoxCount = 0;
                        changeTimer = 0;
                        stateStrange[2].SetActive(false);
                        sight.SetActive(false);
                        previtemCount = 0;
                        itemOn = false;
                    }
                }
            }
        }
    }

    void Toilet()
    {
        // 변기 좌표들 //
        coordinate_y[0] = 0.06f;
        coordinate_y[1] = 1.55f;
        coordinate_z.Add(-2.5f); // 여자 좌변기 우
        coordinate_z.Add(-5.1f); // 여자 좌변기 좌
        coordinate_z.Add(2.4f); // 남자 좌변기 좌
        coordinate_z.Add(5.23f); // 남자 소변기

        float temp_X = -3.7f;
        for (int i = 0; i < 5; ++i) // 1~5    1,2층 여자 남자 , 소변기
        {
            coordinate_x.Add(temp_X);
            temp_X += 0.55f;
        }
        coordinate_x.Add(-2f);  // 6~9 남자화장실 좌변기
        coordinate_x.Add(-2.6f);
        coordinate_x.Add(-3.2f);
        coordinate_x.Add(-3.725f);
        //********************************************************//        

        for (int i = 0; i < 4; ++i)
        {

            int instRand = Random.Range(1, 5);
            // 여자 좌변기 우
            if (instRand != 4)
            {
                toilet_Right.transform.position = new Vector3(coordinate_x[i], coordinate_y[0], coordinate_z[0]);
                Instantiate<Object>(toilet_Right);
            }

            instRand = Random.Range(1, 5);
            if (instRand != 4)
            {
                toilet_Right.transform.position = new Vector3(coordinate_x[i], coordinate_y[1], coordinate_z[0]);
                Instantiate<Object>(toilet_Right);
            }
            instRand = Random.Range(1, 5);
            // 여자 좌변기 좌
            if (instRand != 4)
            {
                toilet_Left.transform.position = new Vector3(coordinate_x[i], coordinate_y[0], coordinate_z[1]);
                Instantiate<Object>(toilet_Left);
            }
            instRand = Random.Range(1, 5);
            if (instRand != 4)
            {
                toilet_Left.transform.position = new Vector3(coordinate_x[i], coordinate_y[1], coordinate_z[1]);
                Instantiate<Object>(toilet_Left);
            }
            instRand = Random.Range(1, 5);
            // 남자 좌변기 좌
            if (instRand != 4)
            {
                toilet_Left.transform.position = new Vector3(coordinate_x[i + 5], coordinate_y[0], coordinate_z[2]);
                Instantiate<Object>(toilet_Left);
            }
            instRand = Random.Range(1, 5);
            if (instRand != 4)
            {
                toilet_Left.transform.position = new Vector3(coordinate_x[i + 5], coordinate_y[1], coordinate_z[2]);
                Instantiate<Object>(toilet_Left);
            }
            instRand = Random.Range(1, 5);
            // 남자 소변기
            if (instRand != 4)
            {
                toilet_Little.transform.position = new Vector3(coordinate_x[i], coordinate_y[0], coordinate_z[3]);
                Instantiate<Object>(toilet_Little);
            }
            instRand = Random.Range(1, 5);
            if (instRand != 4)
            {
                toilet_Little.transform.position = new Vector3(coordinate_x[i], coordinate_y[1], coordinate_z[3]);
                Instantiate<Object>(toilet_Little);
            }
        }
        int _instRand = Random.Range(1, 5);
        // 남자 소변기 5번째 거
        if (_instRand != 4)
        {
            toilet_Little.transform.position = new Vector3(coordinate_x[4], coordinate_y[0], coordinate_z[3]);
            Instantiate<Object>(toilet_Little);
        }
        _instRand = Random.Range(1, 5);
        if (_instRand != 4)
        {
            toilet_Little.transform.position = new Vector3(coordinate_x[4], coordinate_y[1], coordinate_z[3]);
            Instantiate<Object>(toilet_Little);
        }
    }


    //*********************진웅시작**************************8



 
 

}
