﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PlayerInfo
{
    public bool wDown;
    public bool wRun;
    public bool wLeft;
    public bool wRight;
    public bool wBack;
    public bool wAttack;
}