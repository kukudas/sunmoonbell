﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyManager : MonoBehaviour {
    float alphaColor; // 알파값

    public GameObject[] back = new GameObject[3];
    public GameObject _startgame; // UI
    public GameObject _playtag; // 
    public GameObject _exit;
    public Image[] rend = new Image[3]; // UI Renderer
    //public Renderer charRend;

    bool start = false; // 버튼 클릭
    bool nowStart = false; // 알파 변환 끝
	// Use this for initialization
	void Start () {
        rend[0] = _startgame.GetComponent<Image>();
        rend[1] = _playtag.GetComponent<Image>();
        rend[2] = _exit.GetComponent<Image>();
        alphaColor = 1f;
        //charRend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update () {
        //rend[0].color = new Color(rend[0].color.r, rend[0].color.g, rend[0].color.b, alphaColor);
        //rend[1].color = new Color(rend[1].color.r, rend[1].color.g, rend[1].color.b, alphaColor);
        for(int i = 0; i < rend.Length; i++)
        {
            rend[i].color = new Color(rend[i].color.r, rend[i].color.g, rend[i].color.b, alphaColor);

        }

        //charRend.material.color = new Color(charRend.material.color.r, charRend.material.color.g, charRend.material.color.b, alphaColor);

        for (int i = 0; i < 2; ++i)
        {
            back[i].transform.Translate(0.01f, 0, 0);
            if (back[i].transform.localPosition.x > 12.59f)
                back[i].transform.localPosition = new Vector3(-17.00f, 2.76f,-3.8f);
        }

        if(start == true)
        {
            if(alphaColor > 0)
                alphaColor -= 0.01f;

            if(alphaColor <= 0)
                nowStart = true;
        }

        if (nowStart == true)
            Application.LoadLevel("1_StageSelect");
    }

    public void StartButton()
    {
        start = true;
        //Application.LoadLevel("1_School");
    }

    public void ExitButton()
    {
        Application.Quit();
    }
}

