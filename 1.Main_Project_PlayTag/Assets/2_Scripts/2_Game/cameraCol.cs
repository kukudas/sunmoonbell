﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraCol : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
    void OnTriggerEnter(Collider other)
    {
        if(other.tag != "Mimic")
            GameManager.Instance.camera_col = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag != "Mimic")
            GameManager.Instance.camera_col = false;
    }
    // Update is called once per frame
    void Update () {
		
	}
}
