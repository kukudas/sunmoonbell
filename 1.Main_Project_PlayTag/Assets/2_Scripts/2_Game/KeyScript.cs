﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyScript : MonoBehaviour {
    public GameObject effect;
	// Use this for initialization
	void Start () {
		
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (GameManager.Instance.mapType == 0)
            {
                GameManager.Instance.live[GameManager.Instance.liveCount].SetActive(true);
                GameManager.Instance.liveCount++;
                if (GameManager.Instance.liveCount > 4)
                {
                    GameManager.Instance.winAnim = true;

                    effect.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.2f, this.transform.position.z);
                    Instantiate<Object>(effect);

                    for (int i = 0; i < 5; ++i)
                    {
                        GameManager.Instance.live[i].SetActive(false);
                    }
                    GameManager.Instance.liveCount = 0;
                    GameManager.Instance.wallCount++;
                    Debug.Log(GameManager.Instance.wallCount);
                }
            }
            else {
                GameManager.Instance.keyScore++;
            }



            Destroy(gameObject);
        }
    }
    // Update is called once per frame
    void Update () {
        transform.Rotate(0, 1, 0);
	}
}
