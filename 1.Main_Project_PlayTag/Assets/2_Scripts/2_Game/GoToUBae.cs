﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToUBae : MonoBehaviour {

    public Transform Player;
    public GameObject me;

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            GameManager.Instance.tagChange = true;
            Player = col.GetComponent<Transform>();
            GameManager.Instance.charHP[GameManager.Instance.hpCount].SetActive(false);
            GameManager.Instance.hpCount--;
            if (GameManager.Instance.hpCount < 0)
            {
                Player.transform.position = new Vector3(-20f, 10.7894f, 0f);
                GameManager.Instance.ubae = true;
            }
            //Destroy(me);
        }
        //startTimer = Time.time + 0.8f;
        //mimicCollCheck = true;

    }

}
