﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBox : MonoBehaviour {
    // Need for System
    private float selfTimer = 0.0f;
    private float startTimer = 0.0f;
    private float _scale = 0.0f;
    public int _rand;
    public GameManager GM;
    Transform _transform;
    GameObject colChar;
    // Use this for initialization
    void Start () {
        startTimer = Time.time;
        GM = GameManager.Instance;
        _transform = this.transform;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            colChar = other.gameObject;
            GameManager.Instance.changeChar[0] = colChar;
            
            if (GM.itemOn == false)
            {
                if(_rand == 0)
                    _rand = Random.Range(1, 4);
                //_rand = 1;
                GM.itemBoxCount = _rand;

                if (_rand == 5)
                {
                    int rand = Random.Range(1, 6);
                    // 캐릭터 갯수만큼 필요, if(아이템 선정) + if(변신 아이템 이면 그 안에서 또 랜덤)
                    // 점점 흐려지게 하고
                    // 입 다 벌어지면서 + 투명해지면 파괴
                    GM.itemCount = rand;
                }

                Destroy(gameObject);
            }
        }
    }
    // Update is called once per frame
    void Update () {
        if (_transform.localScale.x <= 1f)
        {
            _scale += 0.1f;
            _transform.localScale = new Vector3(_scale, _scale, _scale);
        }

        _transform.Rotate(0, 1, 0);
	}
}
