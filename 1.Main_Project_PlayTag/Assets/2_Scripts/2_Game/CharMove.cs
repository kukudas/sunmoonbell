﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CharMove : MonoBehaviour
{
    // public Animator ani;
    public float moveSpeed;
    public float runSpeed;

    private float tapSpeed = 0.5f;
    private float lastTapTime = 0;
    float turnSpeed;
    Vector3 V3;
    Animator anim;
    float _stamina;

    private bool inputStart = false;
    private float nowTime;// = 12.0f; 
    private float spaceTime;
    private float prevMoveTime = 0.0f;
    private Vector3 prevPosition;
    private Vector3 nowPosition;

    // Key State;
    GameManager key;
    private NetworkView netviewer;

    private bool ubaeNow = false;

    bool tagtag; // tag 핑퐁
    // Use this for initialization
    void Start()
    {
        nowTime = 0;
        // 진웅 추가 
        key = GameManager.Instance;

        // 진웅 추가 끝
        anim = GetComponent<Animator>();
        turnSpeed = 2.0f;
        lastTapTime = 0;
        key.PlayInfo.wDown = false;


        Reset();
        StartCoroutine("updateRoutine");
        Reset();
    }

    // Update is called once per frame

    // 캐릭터 움직임 부분 
    // Mouse X = 캐릭터 좌우 회전
    // WASD 입력 시 누르고 있는 동안 캐릭터 이동 ( 패킷을 계속 보내줘야 할 듯?? )
    // (클라 曰)

    private void Reset()
    {
        if (key.PlayInfo.wRight == true || key.PlayInfo.wLeft == true)
        {
            moveSpeed += 0.5f;
        }
        key.PlayInfo.wDown = false;
        key.PlayInfo.wRun = false;
        key.PlayInfo.wLeft = false;
        key.PlayInfo.wRight = false;
        key.PlayInfo.wBack = false;
        key.PlayInfo.wAttack = false;

    }
    // 키 입력 받는 부분
    IEnumerator updateRoutine()
    {
        while (true)
        {
            


            if (GameManager.Instance.tagChange == true && tagtag == false)
            {
                prevMoveTime = Time.time;
                tagtag = true;
                this.tag = "Untagged";

            }
            if (GameManager.Instance.tagChange == false && GameManager.Instance.goldenBoxOn == false)
                this.tag = "Player";

            if (tagtag == true)
            {

                nowTime = Time.time;
                if (prevMoveTime + 5.0f < nowTime)
                {

                    GameManager.Instance.tagChange = false;
                    tagtag = false;
                }
            }

            if (inputStart == true)
            {
                if (key.ubae == false)
                {
                    /*
                    nowTime = Time.time;
                    if (prevMoveTime + 10f < nowTime)
                    {
                        transform.position = new Vector3(-20f, 10.7894f, 0f);
                        GameManager.Instance.ubae = true;
                    }
                    if (prevMoveTime + 4f < nowTime && remainTime >= 0)
                    {
                        timer.SetActive(true);
                        timer2.SetActive(true);

                        //Debug.Log("남은시간" + (int)(10 - (nowTime - prevMoveTime)));
                        remainTime = 10 - (nowTime - prevMoveTime);
                        timeRemaining.text = ((int)remainTime).ToString();
                        timeRemaining2.text = ((int)remainTime).ToString();

                    }
                    else
                    {
                        timer.SetActive(false);
                        timer2.SetActive(false);

                    }
                    */
                }
            }
            else
            {

            }
            if (inputStart == false)
            {
                if (Input.GetKey(KeyCode.W))
                {
                    inputStart = true;
                    //prevMoveTime = Time.time;
                }
                if (Input.GetKey(KeyCode.A))
                {
                    inputStart = true;
                    //prevMoveTime = Time.time;
                }
                if (Input.GetKey(KeyCode.S))
                {
                    inputStart = true;
                    //prevMoveTime = Time.time;
                }
                if (Input.GetKey(KeyCode.D))
                {
                    inputStart = true;
                    // prevMoveTime = Time.time;
                }
            }
            else
            {
                if (Input.GetKey(KeyCode.W))
                {
                    //prevMoveTime = Time.time;
                }
                if (Input.GetKey(KeyCode.A))
                {
                    //prevMoveTime = Time.time;
                }
                if (Input.GetKey(KeyCode.S))
                {
                    // prevMoveTime = Time.time;
                }
                if (Input.GetKey(KeyCode.D))
                {
                    // prevMoveTime = Time.time;
                }
            }
            if (Input.GetKeyDown(KeyCode.W))
            {
                key.PlayInfo.wDown = true;

                if ((Time.time - lastTapTime) < tapSpeed)
                {
                    if (key.stamina > 0.0f)
                        key.PlayInfo.wRun = true;
                }
                lastTapTime = Time.time;
            }
            if (Input.GetKeyUp(KeyCode.W))
            {

                key.PlayInfo.wDown = false;
                key.PlayInfo.wRun = false;
            }
            if (key.stamina <= 0)
            {
                //wDown = false;
                key.PlayInfo.wRun = false;
            }
            if (key.PlayInfo.wRun == false)
            {
                if (key.stamina <= key.stamina_TimeBar.maxValue)
                    key.stamina += 0.005f;
            }
            if (Input.GetKeyDown(KeyCode.A))
            {

                moveSpeed -= 0.5f;
                key.PlayInfo.wLeft = true;
            }
            if (Input.GetKeyDown(KeyCode.S))
            {

                key.PlayInfo.wBack = true;
            }
            if (Input.GetKeyDown(KeyCode.D))
            {

                moveSpeed -= 0.5f;

                key.PlayInfo.wRight = true;
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                spaceTime = Time.time;
                key.PlayInfo.wAttack = true;
            }
            if (spaceTime + 0.3f < Time.time)
                key.PlayInfo.wAttack = false;

            if (Input.GetKeyUp(KeyCode.A))
            {
                key.PlayInfo.wLeft = false;
                moveSpeed += 0.5f;

            }
            if (Input.GetKeyUp(KeyCode.S))
            {
                key.PlayInfo.wBack = false;

            }
            if (Input.GetKeyUp(KeyCode.D))
            {
                key.PlayInfo.wRight = false;
                moveSpeed += 0.5f;

            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                key.PlayInfo.wAttack = false;

            }
            yield return null;
        }
    }

    // 입력받은 키 바탕으로 실제로 이동 처리
    void FixedUpdate()
    {


        V3 = new Vector3(0, Input.GetAxis("Mouse X"), 0);
        transform.Rotate(V3 * turnSpeed);
        moveSpeed = moveSpeed + key.speedPlus;
        runSpeed = runSpeed + key.speedPlus;
        key.speedPlus = 0;

        Vector3 forward = new Vector3(0, 0, 1); // = Char.transform.TransformDirection(Vector3.forward);
        Vector3 right = new Vector3(1, 0, 0);  //Char.transform.TransformDirection(Vector3.right);


        if (key.PlayInfo.wDown == true)
        {
            anim.SetBool("Walk_F", true);
            transform.Translate(forward * moveSpeed * Time.deltaTime);
        }
        else
            anim.SetBool("Walk_F", false);

        if (key.PlayInfo.wRun == true)
        {
            if (key.stamina > 0)
            {
                key.stamina -= 0.01f;
            }
            anim.SetBool("Run", true);
            if (runSpeed > 3.0f)
                transform.Translate(forward * (runSpeed * 0.5f) * Time.deltaTime);
            else if (runSpeed < 3.0f)
                transform.Translate(forward * (runSpeed * 2f) * Time.deltaTime);
            else
                transform.Translate(forward * runSpeed * Time.deltaTime);

        }
        else
            anim.SetBool("Run", false);

        if (key.PlayInfo.wLeft == true)
        {

            anim.SetBool("Walk_Left", true);


            transform.Translate(right * -1 * moveSpeed * Time.deltaTime);
        }
        else
        {

            anim.SetBool("Walk_Left", false);
        }


        if (key.PlayInfo.wBack == true)
        {
            anim.SetBool("Walk_B", true);

            transform.Translate(forward * -1 * moveSpeed * Time.deltaTime);
        }
        else { anim.SetBool("Walk_B", false); }



        if (key.PlayInfo.wRight == true)
        {
            anim.SetBool("Walk_Right", true);

            transform.Translate(right * moveSpeed * Time.deltaTime);
        }
        else
        {
            anim.SetBool("Walk_Right", false);
        }

        if (key.goldenBoxOn == true)
        {
            this.tag = "Untagged";
            anim.SetBool("Trans", true);
            anim.SetBool("Attack", true);
            key.hammer.SetActive(true);
            key.attackState = true;
        }
        else
        {
            if (GameManager.Instance.tagChange == false)
                this.tag = "Player";
            anim.SetBool("Trans", false);
            anim.SetBool("Attack", false);
            //key.hammer.SetActive(false);
            key.attackState = false;

        }

        if (key.ubae == true)
        {
            anim.SetBool("Scream", true);
            if (ubaeNow == false)
            {
                ubaeNow = true;
                transform.position = new Vector3(-20f, 10.7894f, 0f);

            }
            if (transform.position.y < 9)
            {
                anim.SetBool("Scream", false);
            }
        }


        if (key.PlayInfo.wAttack == true)
        {
            anim.SetBool("Atk", true);
        }
        else
            anim.SetBool("Atk", false);

    }


}
