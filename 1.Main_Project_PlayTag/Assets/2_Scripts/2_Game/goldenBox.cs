﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goldenBox : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.Instance.goldenBoxOn = true;   
        }
    }


    // Update is called once per frame
    void Update () {
        transform.Rotate(0, 1, 0);
	}
}
