﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class childScreenDoor : MonoBehaviour {
    public bool triggerOn;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            triggerOn = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            triggerOn = false ;
        }
    }

}
