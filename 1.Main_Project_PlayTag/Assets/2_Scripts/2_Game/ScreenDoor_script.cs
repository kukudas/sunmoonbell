﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenDoor_script : MonoBehaviour
{

    public bool trigger_on1;
    public bool move_on;

    public float num;
    //Transform _transform;
    public childScreenDoor child;
    // Use this for initialization
    void Start()
    {
        trigger_on1 = true;
        num = 1f;
        //_transform = GetComponentInChildren<Transform>();
        child = GetComponentInChildren<childScreenDoor>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (child.triggerOn == true && trigger_on1 == true)
        {
            move_on = true;
            
        }
        if(trigger_on1 == false && child.triggerOn == false)
        {
            move_on = false;
            
        }

    }
}
