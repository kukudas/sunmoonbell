﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getGoldenKey : MonoBehaviour {
    public GameObject goldKey;
    public GameObject effect;
    float scaleSize = 0.1f;
    public bool tagOn = false;
    float tagTimer;
	// Use this for initialization
	void Start () {
        tagTimer = Time.time+5.0f;
	}
   
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Ground")
            tagOn = true;
        if (other.tag == "hammer")
        {
            goldKey.transform.position = new Vector3(this.transform.position.x, this.transform.position.y+0.2f, this.transform.position.z);
            
            Instantiate<Object>(goldKey);

            effect.transform.position = new Vector3(this.transform.position.x, this.transform.position.y+0.2f, this.transform.position.z);
            Instantiate<Object>(effect);

            Destroy(gameObject);
        }
    }
    // Update is called once per frame
    void Update () {
        if (GameManager.Instance.mapType == 1)
        {
            if (scaleSize < 1.5f)
            {
                transform.localScale = new Vector3(scaleSize, scaleSize, scaleSize);
                scaleSize += 0.01f;
            }
            if (tagOn == true)
                Destroy(gameObject);
        }
	}
}
