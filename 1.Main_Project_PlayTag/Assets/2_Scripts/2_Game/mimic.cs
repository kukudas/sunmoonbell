﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mimic : MonoBehaviour {
    public Transform _temp;
    public float moveSpeed = 2;
    bool mimicCollCheck = false;
    bool afterTIme = false;
    Animator anim;
    Transform _transform;
    // Use this for initialization
    private float selfTimer = 0.0f;
    private float startTimer = 0.0f;
    public void Init()
    {
        anim.SetBool("MFinish", false);
        anim.SetBool("MReady", false);
        anim.SetBool("MRun", false);
        _temp = null;
        mimicCollCheck = false;
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            _temp = col.GetComponent<Transform>();
        }
        startTimer = Time.time+0.8f;
        mimicCollCheck = true;

    }

    void Start () {
        anim = GetComponent<Animator>();
        _transform = this.transform;
        StartCoroutine("updateRoutine");

    }

    // Update is called once per frame
    IEnumerator updateRoutine()
    {
        while (true)
        {
            if (_temp != null)
            {
                if (mimicCollCheck == true)
                {
                    anim.SetBool("MFinish", false);
                    anim.SetBool("MReady", true);
                    anim.SetBool("MRun", true);
                    selfTimer = Time.time;
                    //쫓아가라
                    if (selfTimer > startTimer)
                        afterTIme = true;
                    if(afterTIme == true) {
                        Vector3 Dir = _temp.transform.position - _transform.position;
                        Vector3 NorDir = Dir.normalized;

                        Quaternion angle = Quaternion.LookRotation(NorDir);
                        Debug.Log("angle = " + angle);
                        _transform.rotation = angle;

                        Vector3 Pos = _transform.position;
                        Pos += _transform.forward * Time.smoothDeltaTime * moveSpeed;
                        if (GameManager.Instance.mapType == 0)
                        {
                            if (_transform.position.y < 1f)
                                Pos = new Vector3(Pos.x, 0.06f, Pos.z); // 1층
                            else if (_transform.position.y > 1f && _transform.position.y < 2f)
                                Pos = new Vector3(Pos.x, 1.541f, Pos.z); // 2층
                            else
                                Pos = new Vector3(Pos.x, 3.112f, Pos.z); // 3층
                        }
                        else
                        {
                            Pos = new Vector3(Pos.x, this.transform.position.y, Pos.z);
                        }

                        _transform.position = Pos;
                    }

                }
                else
                {
                    anim.SetBool("MRun", false);
                }


                if ((Mathf.Sqrt((Mathf.Pow(_transform.position.x - _temp.transform.position.x, 2)) +
                   (Mathf.Pow(_transform.position.z - _temp.transform.position.z, 2))) > 2.4f)||GameManager.Instance.tagChange == true)
                {
                    _temp = null;
                    afterTIme = false;
                    mimicCollCheck = false;
                    startTimer = 0.0f;
                    // mimicCollCheck = false;
                    anim.SetBool("MRun", false);
                    anim.SetBool("MFinish", true);
                    anim.SetBool("MReady", false);
                    //Init();

                }

            }
            yield return null;

        }
    }
}
