﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherMove : MonoBehaviour
{
    // public Animator ani;
    public float moveSpeed;
    public float runSpeed;
    private float tapSpeed = 0.5f;
    float turnSpeed;
    Vector3 V3;
    float _stamina;
    Transform _transform;

    // Key State;
    bool wDown;
    bool wRun;
    bool wLeft;
    bool wRight;
    bool wBack;
    // Use this for initialization
    void Start()
    {
        turnSpeed = 2.0f;
        wDown = false;
        _transform = this.transform;
        StartCoroutine("updateRoutine");

    }

    // Update is called once per frame

    // 캐릭터 움직임 부분 
    // Mouse X = 캐릭터 좌우 회전
    // WASD 입력 시 누르고 있는 동안 캐릭터 이동 ( 패킷을 계속 보내줘야 할 듯?? )
    // (클라 曰)


    // 키 입력 받는 부분
    IEnumerator updateRoutine()
    {
        while (true)
        {
            if (GameManager.Instance.mapType != 0)
            {
                if (Input.GetKeyDown(KeyCode.Mouse1))
                {
                    //자유시점

                    GameManager.Instance.mapType = 5;
                }
                else if (Input.GetKeyUp(KeyCode.Mouse1))
                {
                    GameManager.Instance.mapType = 1;
                }
            }

                if (Input.GetKeyDown(KeyCode.W))
                {
                    wDown = true;

                }
                if (Input.GetKeyUp(KeyCode.W))
                {
                    wDown = false;
                }

                //if (GameManager.Instance.itemBoxCount == 5)
                //{
                //    if (GameManager.Instance.stamina <= GameManager.Instance.stamina_TimeBar.maxValue)
                //    {
                //        GameManager.Instance.stamina += 0.005f;
                //    }
                //}
                if (Input.GetKeyDown(KeyCode.A))
                {
                    moveSpeed -= 0.5f;

                    wLeft = true;
                }
                if (Input.GetKeyDown(KeyCode.S))
                {
                    wBack = true;
                }
                if (Input.GetKeyDown(KeyCode.D))
                {
                    moveSpeed -= 0.5f;

                    wRight = true;
                }

                if (Input.GetKeyUp(KeyCode.A))
                {
                    wLeft = false;
                    moveSpeed += 0.5f;

                }
                if (Input.GetKeyUp(KeyCode.S))
                {
                    wBack = false;
                }
                if (Input.GetKeyUp(KeyCode.D))
                {
                    wRight = false;
                    moveSpeed += 0.5f;

                }
            
            yield return null;
        }
    }

    // 입력받은 키 바탕으로 실제로 이동 처리
    void FixedUpdate()
    {

        if (GameManager.Instance.mapType != 5)
        {
            V3 = new Vector3(0, Input.GetAxis("Mouse X"), 0);
            transform.Rotate(V3 * turnSpeed);
            moveSpeed = moveSpeed + GameManager.Instance.speedPlus;
            GameManager.Instance.speedPlus = 0;

            Vector3 forward = new Vector3(0, 0, 1); // = Char.transform.TransformDirection(Vector3.forward);
            Vector3 right = new Vector3(1, 0, 0);  //Char.transform.TransformDirection(Vector3.right);




            if (wDown == true)
            {
                _transform.Translate(forward * moveSpeed * Time.deltaTime);
            }


            if (wLeft == true)
            {
                // ani.SetInteger("Anim", 1);
                _transform.Translate(right * -1 * moveSpeed * Time.deltaTime);
            }

            if (wBack == true)
            {
                // ani.SetInteger("Anim", 1);

                _transform.Translate(forward * -1 * moveSpeed * Time.deltaTime);
            }


            if (wRight == true)
            {
                // ani.SetInteger("Anim", 1);

                _transform.Translate(right * moveSpeed * Time.deltaTime);
            }
        }

        //if (GameManager.Instance.goldenBoxOn == true)
        //{
        //    GameManager.Instance.goldenBoxOn = false;
        //    GameManager.Instance.hammer.SetActive(false);
        //}


    }

}
