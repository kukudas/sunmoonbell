﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    float turnSpeed;
    Vector3 V3;
    Vector3 V3_;
    Transform _transform;
    GameManager key;

    // 자유시점 구현
    bool freeView = false;
    Vector3 prevPos;
    Quaternion prevAngles;
    float mainSpeed = 10.0f; //regular speed
    float shiftAdd = 25.0f; //multiplied by how long shift is held.  Basically running
    float maxShift = 100.0f; //Maximum speed when holdin gshift
    float camSens = 0.1f; //How sensitive it with mouse
    private Vector3 lastMouse = new Vector3(255, 255, 255); //kind of in the middle of the screen, rather than at the top (play)
    private float totalRun = 1.0f;



    // Use this for initialization
    void Start()
    {
        key = GameManager.Instance;
        turnSpeed = 2.0f;
        _transform = this.transform;
        StartCoroutine("updateRoutine");
    }

    // Update is called once per frame
    private Vector3 GetBaseInput()
    { //returns the basic values, if it's 0 than it's not active.
        Vector3 p_Velocity = new Vector3(0, 0, 0);
        if (Input.GetKey(KeyCode.W))
        {
            p_Velocity += new Vector3(0, 0, 1);
        }
        if (Input.GetKey(KeyCode.S))
        {
            p_Velocity += new Vector3(0, 0, -1);
        }
        if (Input.GetKey(KeyCode.A))
        {
            p_Velocity += new Vector3(-1, 0, 0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            p_Velocity += new Vector3(1, 0, 0);
        }
        return p_Velocity;
    }

    // 서버에서 볼 필요 없음 
    IEnumerator updateRoutine()
    {
        while (true)
        {
            if (freeView == true && GameManager.Instance.mapType == 1)
            {
                freeView = false;
                _transform.position = prevPos;
                _transform.rotation = prevAngles;
            }
            V3 = new Vector3(-Input.GetAxis("Mouse Y"), 0, 0);
            _transform.Rotate(V3 * turnSpeed);

            if (GameManager.Instance.mapType != 5)
            {
                ////
                if (key.camera_col == true)
                {
                    if (_transform.localPosition.y > 0.6f)
                    {
                        _transform.Translate(0, -0.001f, 0);
                    }
                    else
                    {
                        _transform.localPosition = new Vector3(0, 0.6f, _transform.localPosition.z);
                    }
                    if (_transform.localPosition.z < -0.35f)
                    {
                        _transform.Translate(0, 0, 0.02f);
                    }
                    else
                    {
                        _transform.localPosition = new Vector3(0, 0.6f, -0.35f);

                    }
                    //this.transform.localPosition = new Vector3(0f, 0.6f, -0.35f);
                }
                else
                {
                    if (GameManager.Instance.mapType == 0)
                    {
                        if (key.itemBoxCount == 1)
                        {
                            if (_transform.localPosition.y < 0.65f)
                            {
                                _transform.Translate(0, 0.001f, 0);
                            }
                            else
                            {
                                _transform.localPosition = new Vector3(0f, 0.65f, _transform.localPosition.z);
                            }
                            if (_transform.localPosition.z > -0.7f)
                            {
                                _transform.Translate(0, 0, -0.02f);
                            }
                            else
                            {
                                _transform.localPosition = new Vector3(0f, 0.65f, -0.7f);
                            }
                        }
                        else
                        {
                            if (_transform.localPosition.y < 0.84f)
                            {
                                _transform.Translate(0, 0.001f, 0);
                            }
                            else
                            {
                                _transform.localPosition = new Vector3(0f, 0.84f, _transform.localPosition.z);
                            }
                            if (_transform.localPosition.z > -0.9f)
                            {
                                _transform.Translate(0, 0, -0.02f);
                            }
                            else
                            {
                                _transform.localPosition = new Vector3(0f, 0.84f, -0.9f);
                            }
                        }
                    }
                }
                //_transform.localPosition = new Vector3(0f, 0.84f, -0.9f);
            }

            else if (GameManager.Instance.mapType == 5) // 자유시점
            {
                if (freeView == false)
                {
                    freeView = true;
                    prevPos = new Vector3(_transform.position.x, _transform.position.y, _transform.position.z);
                    prevAngles = _transform.rotation;
                }

                lastMouse = Input.mousePosition - lastMouse;
                lastMouse = new Vector3(-lastMouse.y * camSens, lastMouse.x * camSens, 0);
                lastMouse = new Vector3(transform.eulerAngles.x + lastMouse.x, transform.eulerAngles.y + lastMouse.y, 0);
                transform.eulerAngles = lastMouse;
                lastMouse = Input.mousePosition;

                float f = 0;
                Vector3 p = GetBaseInput();
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    totalRun += Time.deltaTime;
                    p = p * totalRun * shiftAdd;
                    p.x = Mathf.Clamp(p.x, -maxShift, maxShift);
                    p.y = Mathf.Clamp(p.y, -maxShift, maxShift);
                    p.z = Mathf.Clamp(p.z, -maxShift, maxShift);
                }
                else
                {
                    totalRun = Mathf.Clamp(totalRun * 0.5f, 1, 1000);
                    p = p * mainSpeed;
                }

                p = p * Time.deltaTime;
                if (Input.GetKey(KeyCode.Space))
                {
                    f = transform.position.y;
                    transform.Translate(p);
                    transform.position = new Vector3(transform.position.x, f, transform.position.z);
                }
                else
                {
                    transform.Translate(p);
                }
            }

            yield return null;
        }
    }
}
