﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenMove : MonoBehaviour {
       public float screenSpeed;
    public int timepass;
    Transform _transform;
    ScreenDoor_script parent;
	// Use this for initialization
	void Start ()
    {
        timepass = 0;

        _transform = this.transform;
        parent = GetComponentInParent<ScreenDoor_script>();
	}
	
	// Update is called once per frame
	void Update () {
        if (parent.move_on == true)
        {
            if (timepass < 90)
            {
                timepass += 1;
                screenSpeed = Mathf.Cos(timepass * Mathf.PI / 180);
                _transform.Translate(-screenSpeed / 150, 0, 0);
            }
            else
            {
                parent.trigger_on1 = false;
            }
        }
        else
        {
            if (timepass > 0)
            {
                timepass -= 1;
                screenSpeed = Mathf.Cos((90 - timepass) * Mathf.PI / 180);
                _transform.Translate(screenSpeed / 150, 0, 0);
            }
            else
            {
                parent.trigger_on1 = true;
            }
        }
    }
}
