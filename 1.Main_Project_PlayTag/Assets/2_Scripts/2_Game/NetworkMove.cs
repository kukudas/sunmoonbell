﻿//using UnityEngine;
//using UnityEngine.Networking;


//public class NetworkMove : NetworkBehaviour {

//    [SerializeField]
//    public float moveSpeed = 0.5f;
//    [SerializeField]
//    public float runSpeed;

//    [SerializeField]
//    Transform mainCamera;


//    // Use this for initialization
//    void Start () {
//        if (!isLocalPlayer)
//        {
//            Destroy(this);
//            return;
//        }


//        mainCamera = Camera.main.transform;
//    }

//	// Update is called once per frame
//	void Update () {
//        Vector3 forward = new Vector3(0, 0, 1); // = Char.transform.TransformDirection(Vector3.forward);
//        Vector3 right = new Vector3(1, 0, 0);  //Char.transform.TransformDirection(Vector3.right);



//        if (Input.GetKey(KeyCode.W))
//        {
//            transform.Translate(forward * moveSpeed * Time.deltaTime);
//            mainCamera.Translate(forward * moveSpeed * Time.deltaTime);
//        }
//        if (Input.GetKey(KeyCode.S))
//        {
//            transform.Translate(forward * -1 * moveSpeed * Time.deltaTime);
//            mainCamera.Translate(forward * -1 * moveSpeed * Time.deltaTime);
//        }
//        if (Input.GetKey(KeyCode.A))
//        {
//            transform.Translate(right * -1 * moveSpeed * Time.deltaTime);
//            mainCamera.Translate(right * -1 * moveSpeed * Time.deltaTime);
//        }
//        if (Input.GetKey(KeyCode.D))
//        {
//            transform.Translate(right * moveSpeed * Time.deltaTime);
//            mainCamera.Translate(right * moveSpeed * Time.deltaTime);
//        }

//    }
//}




using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkMove : NetworkBehaviour
{
    [SerializeField]
    public float moveSpeed = 0.5f;
    [SerializeField]
    public float runSpeed;

    [SerializeField]
    Transform mainCamera;
    [SerializeField]
    public GameObject otherObject;

    // public Animator ani;
    private float selfTimer = 0.0f;
    private float startTimer = 0.0f;
    private float tapSpeed = 0.5f;
    private float lastTapTime = 0;
    float turnSpeed;
    Vector3 V3;
    public Animator anim;
    float _stamina;

    private bool inputStart = false;
    private float nowTime;// = 12.0f; 
    private float spaceTime;
    private float prevMoveTime = 0.0f;
    private Vector3 prevPosition;
    private Vector3 nowPosition;
    bool tagtag; // tag 핑퐁

    [SerializeField]
    public bool wDown = false;
    [SerializeField]
    public bool wRun = false;
    [SerializeField]
    public bool wLeft = false;
    [SerializeField]
    public bool wRight = false;
    [SerializeField]
    public bool wBack = false;
    [SerializeField]
    public bool wAttack = false;
    public GameObject hammer;
    private bool testAnim = false;
    private bool ubae = false;
    private bool ubaeNow = false;
    float animTimer;
    public GameObject inst;
    // Use this for initialization
    void Start()
    {
        if (!isLocalPlayer)
        {
            Destroy(this);
            return;
        }
        GameManager.Instance.mine = this.gameObject;

        Debug.Log("start");

        mainCamera = Camera.main.transform;
        mainCamera.transform.parent = this.transform;
        

        nowTime = 0;

        // anim = GetComponent<Animator>();

        turnSpeed = 2.0f;
        lastTapTime = 0;

        selfTimer = Time.time + 2.0f;
        startTimer = Time.time;
        //StartCoroutine("updateRoutine");

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "hammer")
        {
            ubae = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
        // if(NetworkPeerType.Server == Network.peerType)


        if (Input.GetKeyDown(KeyCode.Space))
        {
            hammer.tag = "hammer";
            selfTimer = Time.time + 0.6f;
        }

        if (selfTimer < Time.time)
        {
            hammer.tag = "Untagged";
        }

        if (GameManager.Instance.tagChange == true && tagtag == false)
        {
            prevMoveTime = Time.time;
            tagtag = true;
            this.tag = "Untagged";

        }
        if (GameManager.Instance.tagChange == false && GameManager.Instance.goldenBoxOn == false)
            this.tag = "Player";

        if (tagtag == true)
        {

            nowTime = Time.time;
            if (prevMoveTime + 5.0f < nowTime)
            {

                GameManager.Instance.tagChange = false;
                tagtag = false;
            }
        }

        if(GameManager.Instance.winAnim == true)
        {
            if (testAnim == false)
                animTimer = Time.time + 4f;
            testAnim = true;
        }
        if(animTimer < Time.time)
        {
            testAnim = false;
            GameManager.Instance.winAnim = false;
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            inst.transform.position = new Vector3(this.transform.position.x+2f, this.transform.position.y, this.transform.position.z);
            Instantiate<Object>(inst);
        }
        if (Input.GetKeyUp(KeyCode.P))
            this.tag = "Player";

        if (Input.GetKeyDown(KeyCode.W))
        {
            wDown = true;

            if ((Time.time - lastTapTime) < tapSpeed)
            {
                if (GameManager.Instance.stamina > 0.0f)
                    wRun = true;
            }
            lastTapTime = Time.time;
        }
        if (Input.GetKeyUp(KeyCode.W))
        {

            wDown = false;
            wRun = false;
        }
        if (GameManager.Instance.stamina <= 0)
        {
            //wDown = false;
            wRun = false;
        }
        if (wRun == false)
        {
            if (GameManager.Instance.stamina <= GameManager.Instance.stamina_TimeBar.maxValue)
                GameManager.Instance.stamina += 0.005f;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            moveSpeed -= 0.5f;
            wLeft = true;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            wBack = true;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            moveSpeed -= 0.5f;
            wRight = true;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            spaceTime = Time.time;
            wAttack = true;
        }
        if (spaceTime + 0.3f < Time.time)
            wAttack = false;

        if (Input.GetKeyUp(KeyCode.A))
        {
            wLeft = false;
            moveSpeed += 0.5f;

        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            wBack = false;

        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            wRight = false;
            moveSpeed += 0.5f;

        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            wAttack = false;

        }

    }
    void FixedUpdate()
    {
        V3 = new Vector3(0, Input.GetAxis("Mouse X"), 0);
        Debug.Log(V3);
        transform.Rotate(V3 * turnSpeed);

        moveSpeed = moveSpeed + GameManager.Instance.speedPlus;
        runSpeed = runSpeed + GameManager.Instance.speedPlus;
        //Debug.Log("speed + = " + GameManager.Instance.speedPlus);
        GameManager.Instance.speedPlus = 0;

        Vector3 forward = new Vector3(0, 0, 1); // = Char.transform.TransformDirection(Vector3.forward);
        Vector3 right = new Vector3(1, 0, 0);  //Char.transform.TransformDirection(Vector3.right);

        if (testAnim == true)
            anim.SetBool("Win", true);
        else
            anim.SetBool("Win", false);


        if (wDown == true)
        {
            anim.SetBool("Walk_F", true);
            transform.Translate(forward * moveSpeed * Time.deltaTime);
            //mainCamera.position = new Vector3(mainCamera.position.x + (moveSpeed * Time.deltaTime), mainCamera.position.y, mainCamera.position.z);
        }
        else
            anim.SetBool("Walk_F", false);

        if (wRun == true)
        {
            if (GameManager.Instance.stamina > 0)
            {
                GameManager.Instance.stamina -= 0.01f;
            }
            anim.SetBool("Run", true);
            if (runSpeed > 3.0f)
            {
                transform.Translate(forward * (runSpeed * 0.5f) * Time.deltaTime);
                //mainCamera.position = new Vector3(mainCamera.position.x + ((runSpeed * 0.5f) * Time.deltaTime), mainCamera.position.y, mainCamera.position.z);

                //mainCamera.Translate(forward * (runSpeed * 0.5f) * Time.deltaTime);

            }
            else if (runSpeed < 3.0f)
            {
                transform.Translate(forward * (runSpeed * 2f) * Time.deltaTime);
                //mainCamera.position = new Vector3(mainCamera.position.x + ((runSpeed * 2f) * Time.deltaTime), mainCamera.position.y, mainCamera.position.z);

                //mainCamera.Translate(forward * (runSpeed * 2f) * Time.deltaTime);

            }
            else
            {
                transform.Translate(forward * runSpeed * Time.deltaTime);
                //mainCamera.position = new Vector3(mainCamera.position.x + ((runSpeed) * Time.deltaTime), mainCamera.position.y, mainCamera.position.z);

            }

        }
        else
            anim.SetBool("Run", false);

        if (wLeft == true)
        {
            anim.SetBool("Walk_Left", true);
            transform.Translate(right * -1 * moveSpeed * Time.deltaTime);
            //mainCamera.Translate(right * -1 * moveSpeed * Time.deltaTime);

        }
        else
        {

            anim.SetBool("Walk_Left", false);
        }


        if (wBack == true)
        {
            anim.SetBool("Walk_B", true);

            transform.Translate(forward * -1 * moveSpeed * Time.deltaTime);
            //mainCamera.position = new Vector3(mainCamera.position.x + ((moveSpeed * -1f) * Time.deltaTime), mainCamera.position.y, mainCamera.position.z);

        }
        else { anim.SetBool("Walk_B", false); }



        if (wRight == true)
        {
            anim.SetBool("Walk_Right", true);

            transform.Translate(right * moveSpeed * Time.deltaTime);
            //mainCamera.Translate(right * moveSpeed * Time.deltaTime);

        }
        else
        {
            anim.SetBool("Walk_Right", false);
        }

        if (GameManager.Instance.goldenBoxOn == true)
        {
            this.tag = "Untagged";
            anim.SetBool("Trans", true);
            anim.SetBool("Attack", true);
            GameManager.Instance.hammer.SetActive(true);
            GameManager.Instance.attackState = true;
        }
        else
        {
            if (GameManager.Instance.tagChange == false)
                this.tag = "Player";
            anim.SetBool("Trans", false);
            anim.SetBool("Attack", false);
            //key.hammer.SetActive(false);
            GameManager.Instance.attackState = false;

        }

        if (ubae == true)
        {
            anim.SetBool("Scream", true);
            //transform.position = new Vector3(-20f, 10.7894f, 0f);
            if (ubaeNow == false)
            {
                ubaeNow = true;
                transform.position = new Vector3(-20f, 10.7894f, 0f);
            }
            if (transform.position.y < 9)
            {
                anim.SetBool("Scream", false);
            }
        }


        if (wAttack == true)
        {
            anim.SetBool("Atk", true);
        }
        else
            anim.SetBool("Atk", false);

    }

}

